package main

import (
	"encoding/json"
	"log"
	"net/http"
	"time"

	server "app/pkg/server"
	services "app/pkg/services"

	"github.com/gorilla/mux"
)

//App is the context of the application
type App struct {
	Router *mux.Router
}

//Initialize the context
func (a *App) Initialize() {
	a.Router = mux.NewRouter()

	a.Router.HandleFunc("/baco/rest/fortune", func(w http.ResponseWriter, r *http.Request) {
		// an example API handler
		json.NewEncoder(w).Encode(services.GetFortune())
	})
	a.Router.HandleFunc("/baco/rest/version", func(w http.ResponseWriter, r *http.Request) {
		// an example API handler
		json.NewEncoder(w).Encode(services.GetVersion())
	})

	spa := server.SpaHandler{StaticPath: "static", IndexPath: "index.html"}
	a.Router.PathPrefix("/").Handler(spa)
}

//Run the application
func (a *App) Run() {
	srv := &http.Server{
		Handler: a.Router,
		Addr:    "0.0.0.0:8080",
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Fatal(srv.ListenAndServe())
}

func main() {
	a := App{}
	a.Initialize()
	a.Run()
}
