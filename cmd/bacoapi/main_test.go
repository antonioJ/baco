package main

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

var a App

func TestGetVersion(t *testing.T) {
	a.Initialize()
	req, err := http.NewRequest("GET", "/baco/rest/version", nil)

	if err != nil {
		t.Fatal(err)
	}

	rr := executeRequest(req)
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	expected := "\"go version go1.15.1 linux/amd64\\n\""
	b := rr.Body.String()
	if strings.Compare(b, expected) == 0 {
		t.Errorf("handler returned unexpected body: got %v want %v", b, expected)
	}
}

func TestGetFortune(t *testing.T) {
	a.Initialize()
	req, err := http.NewRequest("GET", "/baco/rest/fortune", nil)

	if err != nil {
		t.Fatal(err)
	}

	rr := executeRequest(req)
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}
	b := rr.Body.String()
	if rr.Body.Len() == 0 {
		t.Errorf("handler returned unexpected body: got %v", b)
	}
}

func TestGetRootPage(t *testing.T) {
	a.Initialize()
	_, err := http.NewRequest("GET", "/", nil)

	if err != nil {
		t.Fatal(err)
	}
}

func executeRequest(req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	a.Router.ServeHTTP(rr, req)

	return rr
}
