package services

import (
	"log"
	"os/exec"
)

// GetFortune gets new fortune cookie
func GetFortune() [1]string {
	cmd := exec.Command("/usr/games/fortune")
	out, err := cmd.CombinedOutput()
	if err != nil {
		log.Fatalf("cmd.Run() failed with %s\n", err)
	}
	fortune := [...]string{string(out)}
	log.Printf("New fotune generated: %sn", fortune)
	return fortune
}

// GetVersion gets the version
func GetVersion() string {
	cmd := exec.Command("go", "version")
	out, err := cmd.CombinedOutput()
	if err != nil {
		log.Fatalf("cmd.Run() failed with %s\n", err)
	}
	version := string(out)
	log.Printf("%sn", version)
	return version
}
