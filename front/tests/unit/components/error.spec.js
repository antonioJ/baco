import { shallowMount } from '@vue/test-utils';
import Error from '@/components/Error.vue';
import errorStore from "@/stores/error.store";

describe('Error.vue', () => {
  it('should create', () => {
    const wrapper = shallowMount(Error, {
      stubs: ['font-awesome-icon']
    });
    expect(wrapper).toBeDefined();
  });
  it('should show error when stored and dismiss', () => {
    const anError = "An error";
    const wrapper = shallowMount(Error, {
      stubs: ['font-awesome-icon']
    });
    expect(wrapper.vm.errorStoreState.errorMessage).toEqual(null);
    errorStore.setErrorMessage(anError);
    expect(wrapper.vm.errorStoreState.errorMessage).toEqual(anError);
    wrapper.vm.dismissError();
    expect(wrapper.vm.errorStoreState.errorMessage).toEqual(null);
  });
});
