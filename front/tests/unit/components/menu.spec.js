import { shallowMount } from '@vue/test-utils';
import Menu from '@/components/Menu.vue';

describe('Menu.vue', () => {
  it('should create', () => {
    const wrapper = shallowMount(Menu, {
      stubs: ['router-link']
    });
    expect(wrapper).toBeDefined();
  });
});
