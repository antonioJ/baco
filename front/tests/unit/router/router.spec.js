import router from '@/router/index';

describe('router index', () => {
  it('should create', () => {
    expect(router).toBeDefined();
  });

  it('should load lazy routes', () => {
    router.options.routes.forEach(element => {
      if (typeof element.component === "function") {
        element.component();
      }
    });
    expect(router).toBeDefined();
  });
});
