export default {
    flushPromises() {
        return new Promise(resolve => setImmediate(resolve));
    }
}