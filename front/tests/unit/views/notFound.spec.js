import { shallowMount } from '@vue/test-utils';
import NotFound from '@/views/NotFound.vue';

describe('NotFound', () => {
  it('should create', () => {
    const wrapper = shallowMount(NotFound);
    expect(wrapper).toBeDefined();
  });
});
