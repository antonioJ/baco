import { shallowMount } from '@vue/test-utils';
import Fortune from '@/views/Fortune.vue';
import axios from 'axios';
import utils from '../utils';
import errorStore from "@/stores/error.store";
import testConstants from "../tests.constants";

jest.mock('axios');

const fortuneUrl = testConstants.restBaseUrl + "/fortune";

afterEach(() => {
  jest.clearAllMocks();
});

describe('Fortune', () => {
  async function testCreatedWithLoadedFortune() {
    const aFortune = "A fortune cookie";
    axios.get.mockImplementationOnce(() => Promise.resolve({ data: [aFortune] }));
    const wrapper = shallowMount(Fortune, {
      stubs: ['font-awesome-icon']
    });
    expect(wrapper).toBeDefined();
    expect(wrapper.vm.fortune).toBeUndefined();
    await utils.flushPromises();
    expect(wrapper.vm.fortune).toEqual(aFortune);
    expect(axios.get).toHaveBeenCalledTimes(1);
    expect(axios.get).toHaveBeenCalledWith(fortuneUrl, { params: { n: 1 } });
    axios.get.mockClear();
    return wrapper;
  }

  it('should create with loaded fortune', async () => {
    await testCreatedWithLoadedFortune();
  });

  it('should create with loaded fortune and reload', async () => {
    const wrapper = await testCreatedWithLoadedFortune();
    const anotherFortune = "Another fortune cookie";
    axios.get.mockImplementationOnce(() => Promise.resolve({ data: [anotherFortune] }));
    wrapper.vm.loadFortune();
    expect(wrapper.vm.fortune).toBeUndefined();
    await utils.flushPromises();
    expect(wrapper.vm.fortune).toEqual(anotherFortune);
    expect(axios.get).toHaveBeenCalledTimes(1);
    expect(axios.get).toHaveBeenCalledWith(fortuneUrl, { params: { n: 1 } });
  });

  it('should create with loaded fortune and detect fail on reload', async () => {
    const wrapper = await testCreatedWithLoadedFortune();
    const anError = "An error";
    axios.get.mockImplementationOnce(() => Promise.reject(new Error(anError)));
    wrapper.vm.loadFortune();
    expect(wrapper.vm.fortune).toBeUndefined();
    await utils.flushPromises();
    expect(wrapper.vm.fortune).toBeUndefined();
    expect(errorStore.state.errorMessage).toEqual(new Error(anError));
    expect(axios.get).toHaveBeenCalledTimes(1);
    expect(axios.get).toHaveBeenCalledWith(fortuneUrl, { params: { n: 1 } });
  });

});
