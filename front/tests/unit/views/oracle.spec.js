import { shallowMount } from '@vue/test-utils';
import Oracle from '@/views/Oracle.vue';
import axios from 'axios';
import utils from '../utils';
import errorStore from "@/stores/error.store";
import testConstants from "../tests.constants";

jest.mock('axios');

const giphyRandom = 'https://api.giphy.com/v1/gifs/random';
const minimalListenTime = 1000;
const maxRelationFactor = 3;

afterEach(() => {
  jest.clearAllMocks();
});

describe('Oracle', () => {

  function expectIdle(wrapper) {
    expect(wrapper.vm.listening).toEqual(false);
    expect(wrapper.vm.loading).toEqual(false);
    expect(wrapper.vm.answer).toEqual(null);
  }

  function expectListening(wrapper) {
    expect(wrapper.vm.listening).toEqual(true);
    expect(wrapper.vm.loading).toEqual(false);
    expect(wrapper.vm.answer).toEqual(null);
  }

  function expectLoading(wrapper) {
    expect(wrapper.vm.listening).toEqual(false);
    expect(wrapper.vm.loading).toEqual(true);
    expect(wrapper.vm.answer).toEqual(null);
  }

  function expectLoaded(wrapper, gifUrl, giphyUrl, author) {
    expect(wrapper.vm.listening).toEqual(false);
    expect(wrapper.vm.loading).toEqual(false);
    expect(wrapper.vm.answer).not.toEqual(null);
    expect(wrapper.vm.answer.gifUrl).toEqual(gifUrl);
    expect(wrapper.vm.answer.giphyUrl).toEqual(giphyUrl);
    expect(wrapper.vm.answer.author).toEqual(author);
  }

  function mockNow(now) {
    jest
      .spyOn(global.Date, 'now')
      .mockImplementationOnce(() => (now));
  }

  function mockGiphy(response) {
    axios.get.mockImplementationOnce(() => Promise.resolve({ data: response }));
    return response;
  }

  it('should create idle', () => {
    const wrapper = shallowMount(Oracle, {
      stubs: ['font-awesome-icon', 'font-awesome-layers']
    });
    expect(wrapper).toBeDefined();
    expectIdle(wrapper);
  });

  it('should not load anything if called too quickly', () => {
    const wrapper = shallowMount(Oracle, {
      stubs: ['font-awesome-icon', 'font-awesome-layers']
    });
    const start = Date.now();
    wrapper.vm.listen();
    expectListening(wrapper);
    mockNow(start + minimalListenTime - 1);
    wrapper.vm.load(1);
    expectIdle(wrapper);
    expect(axios.get).not.toHaveBeenCalled();
  });

  it('should load an answer if called slowly and back to idle', async () => {
    const wrapper = shallowMount(Oracle, {
      stubs: ['font-awesome-icon', 'font-awesome-layers']
    });
    const start = Date.now();
    wrapper.vm.listen();
    expectListening(wrapper);
    mockNow(start + minimalListenTime + 1);
    const aGif = mockGiphy({
      data: {
        image_url: "https://an/url.gif",
        image_height: 10,
        image_width: 10 * (maxRelationFactor * 0.9),
        url: "https://giphy/url",
        user: {
          display_name: "qwerty"
        }
      }
    });
    wrapper.vm.load(1);
    expectLoading(wrapper);
    await utils.flushPromises();
    expectLoaded(wrapper, aGif.data.image_url, aGif.data.url, aGif.data.user.display_name);
    expect(axios.get).toHaveBeenCalledTimes(1);
    wrapper.vm.restart();
    expectIdle(wrapper);
  });

  it('should load an answer if called slowly and back to idle... even if user is not provided', async () => {
    const wrapper = shallowMount(Oracle, {
      stubs: ['font-awesome-icon', 'font-awesome-layers']
    });
    const start = Date.now();
    wrapper.vm.listen();
    expectListening(wrapper);
    mockNow(start + minimalListenTime + 1);
    const aGif = mockGiphy({
      data: {
        image_url: "https://an/url.gif",
        image_height: 10,
        image_width: 10 * (maxRelationFactor * 0.9),
        url: "https://giphy/url"
      }
    });
    wrapper.vm.load(1);
    expectLoading(wrapper);
    await utils.flushPromises();
    expectLoaded(wrapper, aGif.data.image_url, aGif.data.url, undefined);
    expect(axios.get).toHaveBeenCalledTimes(1);
    wrapper.vm.restart();
    expectIdle(wrapper);
  });


  it('should retry ratios too large or small', async () => {
    const wrapper = shallowMount(Oracle, {
      stubs: ['font-awesome-icon', 'font-awesome-layers']
    });
    const start = Date.now();
    wrapper.vm.listen();
    expectListening(wrapper);
    mockNow(start + minimalListenTime + 1);
    mockGiphy({
      data: {
        image_url: "https://a/url.gif",
        image_height: 10,
        image_width: 10 * (maxRelationFactor * 1.1),
        user: {
          display_name: "qwerty"
        }
      }
    });
    mockGiphy({
      data: {
        image_url: "https://another/url.gif",
        image_height: 10 * (maxRelationFactor * 1.1),
        image_width: 10,
        user: {
          display_name: "qwerty"
        }
      }
    });
    const correctRatioGif = mockGiphy({
      data: {
        image_url: "https://oneLast/url.gif",
        image_height: 10,
        image_width: 10 * (maxRelationFactor * 0.9),
        user: {
          display_name: "qwerty"
        }
      }
    });
    wrapper.vm.load(1);
    expectLoading(wrapper);
    await utils.flushPromises();
    expectLoaded(wrapper, correctRatioGif.data.image_url, correctRatioGif.data.url, correctRatioGif.data.user.display_name);
    expect(axios.get).toHaveBeenCalledTimes(3);
  });

  it('should detect error', async () => {
    const wrapper = shallowMount(Oracle, {
      stubs: ['font-awesome-icon', 'font-awesome-layers']
    });
    const start = Date.now();
    wrapper.vm.listen();
    expectListening(wrapper);
    mockNow(start + minimalListenTime + 1);
    const anError = "An error";
    axios.get.mockImplementationOnce(() => Promise.reject(new Error(anError)));
    wrapper.vm.load(1);
    expectLoading(wrapper);
    await utils.flushPromises();
    expect(errorStore.state.errorMessage).toEqual(new Error(anError));
    expectIdle(wrapper);
    expect(axios.get).toHaveBeenCalledTimes(1);
  });


});
