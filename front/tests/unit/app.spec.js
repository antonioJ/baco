import { shallowMount } from '@vue/test-utils';
import App from '@/App.vue';
import axios from 'axios';
import utils from './utils';
import testConstants from "./tests.constants";

jest.mock('axios');

const versionUrl = testConstants.restBaseUrl + "/version";

afterEach(() => {
  jest.clearAllMocks();
});

describe('App', () => {
  async function testCreatedWithLoadedVersion() {
    const aVersion = "1.2.3";
    axios.get.mockImplementationOnce(() => Promise.resolve({ data: aVersion }));
    const wrapper = shallowMount(App, {
      stubs: ['router-link', 'router-view', 'font-awesome-icon']
    });
    expect(wrapper).toBeDefined();
    expect(wrapper.vm.version).toBeUndefined();
    await utils.flushPromises();
    expect(wrapper.vm.version).toEqual(aVersion);
    expect(axios.get).toHaveBeenCalledTimes(1);
    expect(axios.get).toHaveBeenCalledWith(versionUrl);
    return wrapper;
  }

  it('should create with version', async () => {
    await testCreatedWithLoadedVersion();
  });

  it('should toggle menu', async () => {
    const wrapper = await testCreatedWithLoadedVersion();
    expect(wrapper.vm.menuVisible).toEqual(false);
    wrapper.vm.toggleMenu();
    expect(wrapper.vm.menuVisible).toEqual(true);
    wrapper.vm.toggleMenu();
    expect(wrapper.vm.menuVisible).toEqual(false);
  });
});


