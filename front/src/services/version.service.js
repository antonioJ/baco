import axios from "axios";
import constants from "./service.constants"

const url = constants.restBaseUrl + "/version";
export default {
    async getVersion() {
        return axios
            .get(url)
            .then(response => {
                return response.data;
            });
    }
}