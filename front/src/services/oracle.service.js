import axios from "axios";
import OracleAnswer from "./OracleAnswer";

const giphyRandom = 'https://api.giphy.com/v1/gifs/random';
const giphyApiKey = '74g5KeZbM2ZlREjWYLxEPOlPayx0xH4J';
const maxRelationFactor = 3;
const minRelationFactor = 1 / maxRelationFactor;

const answers = ["Yes", "No", "Maybe"];

export default {
    async getResponse(requestedRatio) {
        const answerNumber = Math.floor(Math.random() * answers.length);
        const answer = answers[answerNumber];
        return axios
            .get(giphyRandom,
                {
                    params:
                    {
                        api_key: giphyApiKey,
                        tag: answer,
                        limit: 1
                    }
                })
            .then(response => {
                const gotRatio = response.data.data.image_height / response.data.data.image_width;
                const relation = requestedRatio / gotRatio;
                if ((relation > maxRelationFactor) || (relation < minRelationFactor)) {
                    console.log("Discarded gif with aspect ration relation " + relation);
                    return this.getResponse(requestedRatio);
                } else {
                    console.log("Choose gif with aspect ratio relation " + relation);
                    return new OracleAnswer(
                        answer,
                        response.data.data.image_url,
                        response.data.data.url,
                        response.data.data.user?.display_name
                    );
                }
            });
    }
}