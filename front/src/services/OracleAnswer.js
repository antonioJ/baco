export default class OracleAnswer {
    constructor(answer, gifUrl, giphyUrl, author) {
        this.answer = answer;
        this.gifUrl = gifUrl;
        this.giphyUrl = giphyUrl;
        this.author = author;
    }
}