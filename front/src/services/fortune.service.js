import axios from "axios";
import constants from "./service.constants"

const url = constants.restBaseUrl + "/fortune";
export default {
    async getFortune() {
        return axios
            .get(url, { params: { n: 1 } })
            .then(response => {
                return response.data[0];
            });
    }
}