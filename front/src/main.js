import Vue from 'vue';
import App from './App.vue';
import router from './router';
import { FontAwesomeIcon, FontAwesomeLayers } from "@fortawesome/vue-fontawesome";

Vue.config.productionTip = false;

Vue.component("font-awesome-icon", FontAwesomeIcon);
Vue.component('font-awesome-layers', FontAwesomeLayers);

new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app');
