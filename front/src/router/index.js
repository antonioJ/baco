import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Root',
    redirect: '/fortune',
  },
  {
    path: '/fortune',
    name: 'Fortune',
    component: () => import(/* webpackChunkName: "fortune" */ '../views/Fortune.vue'),
  },
  {
    path: '/oracle',
    name: 'Oracle',
    component: () => import(/* webpackChunkName: "oracle" */ '../views/Oracle.vue'),
  },
  {
    path: '*',
    name: 'NotFound',
    component: () => import(/* webpackChunkName: "notFound" */ '../views/NotFound.vue'),
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
