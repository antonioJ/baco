export default {
    state: {
        errorMessage: null
    },
    setErrorMessage(errorMessage) {
        this.state.errorMessage = errorMessage;
    },
    clearErrorMessage() {
        this.setErrorMessage(null);
    }
}