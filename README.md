# Baco

Keywords: go, golang, vue, vscode, devcontainer, docker, giphy

Reference architecture of public web application based on [Oruga](https://gitlab.com/javier.sedano/oruga) proyect from [Javier Sedano](https://gitlab.com/javier.sedano). Thanks Javi.

Frontend created using Vue.JS by [Javier Sedano](https://gitlab.com/javier.sedano).
Backend created in Go.

[![Build Status](https://gitlab.com/antonioJ/baco/badges/master/pipeline.svg)](https://gitlab.com/antonioJ/baco/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/antonioJ/baco)](https://goreportcard.com/badge/gitlab.com/antonioJ/baco)

Instructions:
* Suggested environment:
  * Visual Studio Code. Plugins:
    * Docker
    * Remote - Containers
  * Docker. Docker Desktop for Windows is tested
* Clone from git
* Open folder in VSCode
  * Remote container will be detected, and offered to "Reopen in container". Do so.
* Makefile targets will be shown in Task Explorer
  * Run "dependenciesInit" to download dependencies (command line: make dependenciesInit)
  * Run "buildFront" to build the frontend (command line: make buildFront)
* Press F5 to start debugging
* Browse http://localhost:5000/