ifeq ($(VERSION),)
	VERSION := 0.0.0-dev
endif
PVERSION=/p:Version=$(VERSION)

dependenciesInit: goDependencies
	cd front && npm run dependenciesInit && cd ..

goDependencies:
	go get -v -d ./cmd/... && go get -v -d ./pkg/...

buildGo: goDependencies
	go build -i -v ./cmd/bacoapi/main.go

clean:
	cd front && npm run clean && cd ..

buildFront:
	cd front && npm run build -- --dest=../cmd/bacoapi/static && cd ..
	
buildFrontWatch:
	cd front && npm run build -- --mode=development --watch --dest=../cmd/bacoapi/static && cd ..

test: ## Run unittests
	go test -short ./cmd/...

race: ## Run data race detector
	go test -race -short ./cmd/...

deploy: dependenciesInit
	mkdir myArtifact && go build -o ./myArtifact/app -i -v ./cmd/bacoapi/main.go && cd front && npm run build -- --dest=../myArtifact/static && cd ..
